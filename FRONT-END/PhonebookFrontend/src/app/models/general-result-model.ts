import { IResult } from "../interfaces/iresult";

  export class GeneralResult implements IResult {
    result: any|null; //GENERIC
    userMessage :       string='';
    success :           boolean=false;
    httpStatusCode :    number=0;
   
  }