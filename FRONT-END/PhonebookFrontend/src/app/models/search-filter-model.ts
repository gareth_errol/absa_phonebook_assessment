export class SearchFilterModel{
    sortBy :         string = 'name';
    pageSize :       number = 5;
    pageNumber :     number = 1;
    name :           string = '';
    number :         string = '';
    phoneBookId :    number = 0;
}