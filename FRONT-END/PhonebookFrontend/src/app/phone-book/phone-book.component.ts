import { Component, OnInit } from '@angular/core';
import { EntryPassModel } from '../models/entry-pass-model';
import { GeneralResult } from '../models/general-result-model';
import { PhoneBookAddModel } from '../models/phone-book-add-model';
import { PhoneBookPassModel } from '../models/phone-book-pass-model';
import { SearchFilterModel } from '../models/search-filter-model';
import { EntryService } from '../services/entry-service.service';
import { NotificationsService } from '../services/notifications.service';
import { PhonebookService } from '../services/phonebook-service.service';

@Component({
  selector: 'app-phone-book',
  templateUrl: './phone-book.component.html',
  styleUrls: ['./phone-book.component.css']
})
export class PhoneBookComponent implements OnInit {
  phoneBook:PhoneBookAddModel = new PhoneBookAddModel();
  phoneBookSearch:SearchFilterModel=new SearchFilterModel();
  phoneBookSearchResult: GeneralResult = new GeneralResult();
  nextPhoneBookEnabled: boolean = true;
  prevPhoneBookEnabled: boolean = true;

  entrySearchResult: GeneralResult = new GeneralResult();
  entrySearch: SearchFilterModel = new SearchFilterModel();
  entry: EntryPassModel = new EntryPassModel();
  phonebooks: PhoneBookPassModel[] = [];
  nextEnabled: boolean = true;
  prevEnabled: boolean = true;

  constructor(
    private entryService: EntryService,
    private phoneBookService: PhonebookService,
    private notification: NotificationsService
  ) { }

  ngOnInit() {
    this.phoneBookService.GetPhonebooks().subscribe(res => {
      if (res.success) {
        this.phonebooks = res.result;
      }
      else {
        this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
      }
    });;

  }

  //PHONEBOOKS
  addPhoneBook(){
    //this.notification.showInfoTimer("Adding record, please wait...", "Adding...",2);
    this.phoneBookService.AddPhoneBook(this.phoneBook).subscribe(res => {
      if (res.success) {
        this.notification.showSuccessTimer(res.userMessage, "Success", 5);
        this.phoneBookService.GetPhonebooks().subscribe(res => {
          if (res.success) {
            this.phonebooks = res.result;
          }
          else {
            this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
          }
        });;
      }
      else {
        this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
      }
    });
  }

  searchPB() {
    // this.notification.showInfoTimer("Search started, please wait...", "Searching...",2);
     this.phoneBookSearchResult= new GeneralResult();
     this.phoneBookService.SearchPhoneBooks(this.phoneBookSearch).subscribe(res => {
       if (res.success) {
         this.phoneBookSearchResult = res;
         this.notification.showSuccessTimer(res.userMessage, "Success", 5);
         if (this.phoneBookSearchResult.result.length >= this.phoneBookSearch.pageSize) {
           this.nextPhoneBookEnabled = false;
         }
         else {
           this.nextPhoneBookEnabled = true;
           if (this.phoneBookSearch.pageNumber > 1) {
             this.prevPhoneBookEnabled = false;
           }
         }
       }
       else {
         this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
       }
     });
   }
 
   searchPhoneBooks() {
     this.prevPhoneBookEnabled = true;
     this.nextPhoneBookEnabled = true;
     this.phoneBookSearch.pageNumber = 1;
     this.searchPB();
   }
 
   pagePhoneBookNext() {
     this.phoneBookSearch.pageNumber++;
     this.searchPB();
     this.prevPhoneBookEnabled = false;
   }
 
   pagePhoneBookPrev() {
     if (this.phoneBookSearch.pageNumber > 1) {
       this.phoneBookSearch.pageNumber--;
     }
     else {
       this.prevPhoneBookEnabled = true;
     }
     this.searchPB();
   }

   deletePhoneBook(id:number)
   {
    this.phoneBookService.DeletePhoneBook(id).subscribe(res => {
      if (res.success) {
        this.notification.showSuccessTimer(res.userMessage, "Success", 5);
        this.searchPB();
      }
      else {
        this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
      }
    });
   }
  //ENTRIES
  addNewEntry() {
    //this.notification.showInfoTimer("Adding record, please wait...", "Adding...",2);
    this.entryService.AddEntry(this.entry).subscribe(res => {
      if (res.success) {
        this.notification.showSuccessTimer(res.userMessage, "Success", 5);
      }
      else {
        this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
      }
    });
  }

  deleteEntry(id:number) {
   // this.notification.showInfoTimer("Deleting record, please wait...", "Deleting...",2);
    this.entryService.DeleteEntry(id).subscribe(res => {
      if (res.success) {
        this.notification.showSuccessTimer(res.userMessage, "Success", 5);
        this.search();
      }
      else {
        this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
      }
    });
  }  

  search() {
   // this.notification.showInfoTimer("Search started, please wait...", "Searching...",2);
    this.entrySearchResult= new GeneralResult();
    this.entryService.SearchEntries(this.entrySearch).subscribe(res => {
      if (res.success) {
        this.entrySearchResult = res;
        this.notification.showSuccessTimer(res.userMessage, "Success", 5);
        if (this.entrySearchResult.result.length >= this.entrySearch.pageSize) {
          this.nextEnabled = false;
        }
        else {
          this.nextEnabled = true;
          if (this.entrySearch.pageNumber > 1) {
            this.prevEnabled = false;
          }
        }
      }
      else {
        this.notification.showErrorTimer(res.userMessage, "ERROR", 5);
      }
    });
  }

  searchEntries() {
    this.prevEnabled = true;
    this.nextEnabled = true;
    this.entrySearch.pageNumber = 1;
    this.search();
  }

  pageNext() {
    this.entrySearch.pageNumber++;
    this.search();
    this.prevEnabled = false;
  }

  pagePrev() {
    if (this.entrySearch.pageNumber > 1) {
      this.entrySearch.pageNumber--;
    }
    else {
      this.prevEnabled = true;
    }
    this.search();
  }

}
