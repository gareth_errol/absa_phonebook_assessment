export interface IResult {
    userMessage :       string;
    success :           boolean;
    httpStatusCode :    number;
}