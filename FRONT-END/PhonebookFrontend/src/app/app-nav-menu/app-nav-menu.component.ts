import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
declare var jQuery: any;
@Component({
  selector: 'app-nav-menu',
  templateUrl: './app-nav-menu.component.html',
  styleUrls: ['./app-nav-menu.component.css']
})
export class AppNavMenuComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Inject('WEB_URL') private webUrl: string,
  ) {}

  ngOnInit(): void {
  }

}
