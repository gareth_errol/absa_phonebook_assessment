import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { EntryPassModel } from '../models/entry-pass-model';
import { GeneralResult } from '../models/general-result-model';
import { NotificationsService } from './notifications.service';
import { map, catchError } from 'rxjs/operators';
import { SearchFilterModel } from '../models/search-filter-model';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  constructor(
    private http: HttpClient,
    private notification: NotificationsService,
    @Inject('BASE_URL') private baseUrl: string
  ) { }


  AddEntry(entry: EntryPassModel): Observable<GeneralResult> {
    return this.http.post(this.baseUrl + 'api/entry', entry, { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  DeleteEntry(id: number): Observable<GeneralResult> {
    return this.http.delete(this.baseUrl + 'api/entry/' + id.toString(), { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  SearchEntries(search: SearchFilterModel): Observable<GeneralResult> {
    let url = this.searchUrlBuilder(search);
    return this.http.get(this.baseUrl + 'api/entry' + url, { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  GetPhonebooks(): Observable<GeneralResult> {
    let searchResult: GeneralResult = new GeneralResult();
    return this.http.get<GeneralResult>(this.baseUrl + 'api/phonebook/all', { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  private extractData(res: any) {
    let body = res;
    return body;
  }

  private handleErrorObservable(error: HttpErrorResponse) {
    if (error.error.title != null) {
      alert(error.error.title);
    }
    if(error.error.userMessage != null)
    {
      alert(error.error.userMessage);
    }
    return throwError(error);
  }

  private searchUrlBuilder(search: SearchFilterModel) {
    let url = '?sortBy=name&pageSize=' + search.pageSize + "&pageNumber=" + search.pageNumber;
    if (search.name !== '') {
      url = url + "&name=" + search.name;
    }
    if (search.number !== '') {
      url = url + "&number=" + search.number;
    }
    if (search.phoneBookId > 0) {
      url = url + "&phoneBookId=" + search.phoneBookId;
    }
    return url;
  }

  //NOT IMPLEMENTED
  UpdateEntry(entry: EntryPassModel, id: number): Observable<GeneralResult> {
    return this.http.post(this.baseUrl + 'api/entry/' + id.toString(), entry, { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

}

