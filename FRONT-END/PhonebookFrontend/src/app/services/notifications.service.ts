import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private toost: ToastrService) {}

  clearToasts(){
    this.toost.clear();
  }

  showSuccess(message: string, title: string) {
    this.toost.success(message, title);
  }

  showSuccessTimer(message: string, title: string, seconds: number) {
    this.toost.success(message, title, {
      timeOut: seconds * 1000,
    });
  }

  showInfo(message: string, title: string) {
    this.toost.info(message, title);
  }

  showInfoTimer(message: string, title: string, seconds: number) {
    this.toost.info(message, title, {
      timeOut: seconds * 1000,
    });
  }

  showWarning(message: string, title: string) {
    this.toost.warning(message, title);
  }

  showWarningTimer(message: string, title: string, seconds: number) {
    this.toost.warning(message, title, {
      timeOut: seconds * 1000,
    });
  }

  showError(message: string, title: string) {
    this.toost.error(message, title);
  }

  showErrorTimer(message: string, title: string, seconds: number) {
    this.toost.error(message, title, {
      timeOut: seconds * 1000,
    });
  }
}
