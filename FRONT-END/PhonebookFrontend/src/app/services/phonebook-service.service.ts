import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GeneralResult } from '../models/general-result-model';
import { PhoneBookAddModel } from '../models/phone-book-add-model';
import { SearchFilterModel } from '../models/search-filter-model';
import { NotificationsService } from './notifications.service';

@Injectable({
  providedIn: 'root'
})
export class PhonebookService {

  constructor(
    private http: HttpClient,
    private notification: NotificationsService,
    @Inject('BASE_URL') private baseUrl: string
  ) { }

  GetPhonebooks(): Observable<GeneralResult> {
    let searchResult: GeneralResult = new GeneralResult();
    return this.http.get<GeneralResult>(this.baseUrl + 'api/phonebook/all', { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  AddPhoneBook(phonebook: PhoneBookAddModel): Observable<GeneralResult> {
    return this.http.post(this.baseUrl + 'api/phonebook', phonebook, { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  DeletePhoneBook(id: number): Observable<GeneralResult> {
    return this.http.delete(this.baseUrl + 'api/phonebook/' + id.toString(), { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  SearchPhoneBooks(search: SearchFilterModel): Observable<GeneralResult> {
    let url = this.searchUrlBuilder(search);
    return this.http.get(this.baseUrl + 'api/phonebook' + url, { observe: 'response' }).pipe(
      map(data => {
        return this.extractData(data.body);
      }),
      catchError(this.handleErrorObservable)
    );
  }

  private extractData(res: any) {
    let body = res;
    return body;
  }

  private handleErrorObservable(error: HttpErrorResponse) {
    if (error.error.title != null) {
      alert(error.error.title);
    }
    if (error.error.userMessage != null) {
      alert(error.error.userMessage);
    }
    return throwError(error);
  }

  private searchUrlBuilder(search: SearchFilterModel) {
    let url = '?sortBy=name&pageSize=' + search.pageSize + "&pageNumber=" + search.pageNumber;
    if (search.name != '') {
      url = url + "&name=" + search.name;
    }
    return url;
  }
}
