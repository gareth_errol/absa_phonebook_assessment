﻿using PhoneBook.Core.Services.Interfaces;
using PhoneBook.Core.Services.ServiceModels;
using PhoneBook.Data.DataContext;
using PhoneBook.Data.Helpers;
using PhoneBook.Data.Repositories;
using PhoneBook.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Core.Services.Implementation
{
    public class PhoneBookService : IPhoneBookService
    {
        private readonly DefaultDatabaseContext _context;
        IPhoneBookRepository _phoneBookRepository;

        public PhoneBookService(DefaultDatabaseContext context)
        {
            _context = context;
            _phoneBookRepository = new PhoneBookRepository(_context);
        }

        public PhoneBookReturnObject<List<PhoneBookModel>> GetAllPhoneBooks()
        {
            //Init return object
            PhoneBookReturnObject<List<PhoneBookModel>> result = new PhoneBookReturnObject<List<PhoneBookModel>>();
            try
            {
                result.result = new List<PhoneBookModel>();
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    //Delete the phone number entry for the specified entry unique identifier
                    foreach (var pb in _context.PhoneBooks)
                    {
                        result.result.Add(new PhoneBookModel
                        {
                            id = pb.id,
                            phoneBookName = pb.name
                        });
                    }

                }
                result.success = true;
                result.userMessage = "Fetched all phone books";
                result.httpStatusCode = 200;
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to delete the provided entry.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<PhoneBookReturnObject<PhoneBookModel>> AddPhoneBook(GeneralPhoneBookModel newPhoneBook)
        {
            //Init return object
            PhoneBookReturnObject<PhoneBookModel> result = new PhoneBookReturnObject<PhoneBookModel>();
            try
            {
                //Purposely Disposing the connection in the using blocks
                using (_context)
                {
                    //Check if the phone book name is already in use
                    bool phoneBookExists = _phoneBookRepository.PhoneBookNameExists(newPhoneBook.phoneBookName);
                    switch (phoneBookExists)
                    {
                        //The method has not failed or thrown an error, but the phone book name provided already exists in the system
                        case true:
                            result.userMessage = "There is already a phone book in the system with the same name that you have provided, please provide another name.";
                            result.httpStatusCode = 409;
                            break;
                        //The phone book does not exist. Add to the database
                        case false:
                            var added = await _phoneBookRepository.Create(new Data.Entities.PhoneBook
                            {
                                name = newPhoneBook.phoneBookName
                            });
                            //The phone book was added successfully, prep the return object with the necessary required fields.
                            result.success = true;
                            result.userMessage = "Your new phone book was successfully added to the system.";
                            //TODO: MANUAL MAPPING - IF HAVE TIME USE AUTOMAPPER
                            result.result = new PhoneBookModel
                            {
                                id = added.id,
                                phoneBookName = added.name
                            };
                            result.httpStatusCode = 200;
                            break;
                    }
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to add this phone book to the system.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<PhoneBookReturnObject<PhoneBookModel>> UpdatePhoneBook(GeneralPhoneBookModel updatedPhoneBook, int phoneBookId)
        {
            //Init return object
            PhoneBookReturnObject<PhoneBookModel> result = new PhoneBookReturnObject<PhoneBookModel>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    //Check if the phone book exists
                    Data.Entities.PhoneBook phoneBook = await _phoneBookRepository.Read(phoneBookId);
                    if (phoneBook == null)
                    {
                        //The phone book does not exist. Preps result accordingly.
                        result.userMessage = "The phone book you are trying to update does not exist.";
                        result.httpStatusCode = 400;
                    }
                    else
                    {
                        //The phone book does exist. Map the passed values to the current phone book.
                        phoneBook.name = updatedPhoneBook.phoneBookName;
                        //Update the phone book entry and prep return result.
                        phoneBook = await _phoneBookRepository.Update(phoneBook, phoneBook.id);
                        result.success = true;
                        result.userMessage = "Entry updated successfully.";
                        //MANUAL MAPPING - IF HAVE TIME USE AUTOMAPPER
                        result.result = new PhoneBookModel
                        {
                            id = phoneBook.id,
                            phoneBookName = phoneBook.name
                        };
                        result.httpStatusCode = 200;
                    }
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to update the provided phone book.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<PhoneBookReturnObject<PhoneBookModel>> DeleteEntryPhoneBook(int phoneBookId)
        {
            //Init return object
            PhoneBookReturnObject<PhoneBookModel> result = new PhoneBookReturnObject<PhoneBookModel>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    //Delete the phone number entry for the specified entry unique identifier
                    switch (await _phoneBookRepository.Delete(phoneBookId))
                    {
                        //The deletion was successful.
                        case true:
                            //Prep result object accordingly.
                            result.success = true;
                            result.userMessage = "The specified phone book was deleted successfully.";
                            result.httpStatusCode = 200;
                            break;
                        //The deletion was unable to take place as the specified unique identier does not exist. 
                        case false:
                            //Prep result object accordingly.
                            result.userMessage = "The phone book you are trying to delete does not exist.";
                            result.httpStatusCode = 400;
                            break;
                    }
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to delete the provided entry.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<PhoneBookReturnObject<PagedList<Data.Entities.PhoneBook>>> SearchPhoneBooks(string sortBy, Dictionary<string, string> searchCriteria, int pageNumber, int pageSize)
        {
            PhoneBookReturnObject<PagedList<Data.Entities.PhoneBook>> result = new PhoneBookReturnObject<PagedList<Data.Entities.PhoneBook>>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    result.result = (await _phoneBookRepository.SearchPhoneBooks(sortBy, searchCriteria, pageNumber, pageSize));
                    result.success = true;
                    result.userMessage = "Completed search.";
                    result.httpStatusCode = 200;
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error find the entries you are looking for.";
                result.httpStatusCode = 500;
            }
            return result;
        }
    }
}
