﻿using PhoneBook.Core.Services.Interfaces;
using PhoneBook.Core.Services.ServiceModels;
using PhoneBook.Data.DataContext;
using PhoneBook.Data.Entities;
using PhoneBook.Data.Helpers;
using PhoneBook.Data.Repositories;
using PhoneBook.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Core.Services.Implementation
{
    public class EntryService : IEntryService
    {
        private readonly DefaultDatabaseContext _context;
        IEntryRepository _entryRepository;

        public EntryService(DefaultDatabaseContext context)
        {
            _context = context;
            _entryRepository = new EntryRepository(_context);
        }

        public async Task<EntryReturnObject<EntryModel>> AddEntryItem(GeneralEntryModel newEntry)
        {
            //Init return object
            EntryReturnObject<EntryModel> result = new EntryReturnObject<EntryModel>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    //Check if the phone number entry already exists in the specified phone book
                    bool numberExists = _entryRepository.NumberExists(newEntry.entryPhoneBookId, newEntry.entryPhoneNumber);
                    switch (numberExists)
                    {
                        //The method has not failed or thrown an error, but the phone number provided already exists in the specified phone book
                        case true:
                            result.userMessage = "The number you are trying to add already exists in the selected phone book.";
                            result.httpStatusCode = 409;
                            break;
                        //The phone number entry does not exist. Add to the database
                        case false:
                            var added = await _entryRepository.Create(new Entry
                            {
                                name = newEntry.entryName,
                                phoneNumber = newEntry.entryPhoneNumber,
                                phoneBookId = newEntry.entryPhoneBookId
                            });
                            //The phone number entry was added successfully, prep the return object with the necessary required fields.
                            result.success = true;
                            result.userMessage = "Number added successfully to the selected phone book.";
                            //TODO: MANUAL MAPPING - IF HAVE TIME USE AUTOMAPPER
                            result.result = new EntryModel
                            {
                                id = added.id,
                                entryName = added.name,
                                entryPhoneBookId = added.phoneBookId,
                                entryPhoneNumber = added.phoneNumber
                            };
                            result.httpStatusCode = 200;
                            break;
                    }
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to add this entry to the system.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<EntryReturnObject<EntryModel>> UpdateEntryItem(GeneralEntryModel updateEntry, int entryId)
        {
            //Init return object
            EntryReturnObject<EntryModel> result = new EntryReturnObject<EntryModel>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    //Check if the phone number entry exists
                    Entry entry = await _entryRepository.Read(entryId);
                    if (entry == null)
                    {
                        //The phone number entry does not exist. Preps result accordingly.
                        result.userMessage = "The entry you are trying to update does not exist.";
                        result.httpStatusCode = 400;
                    }
                    else
                    {
                        //The phone number entry does exist. Map the passed values to the current entry.
                        entry.name = updateEntry.entryName;
                        entry.phoneBookId = updateEntry.entryPhoneBookId;
                        entry.phoneNumber = updateEntry.entryPhoneNumber;
                        //Update the phone number entry and prep return result.
                        entry = await _entryRepository.Update(entry, entry.id);
                        result.success = true;
                        result.userMessage = "Entry updated successfully.";
                        //MANUAL MAPPING - IF HAVE TIME USE AUTOMAPPER
                        result.result = new EntryModel
                        {
                            id = entry.id,
                            entryName = entry.name,
                            entryPhoneBookId = entry.phoneBookId,
                            entryPhoneNumber = entry.phoneNumber
                        };
                        result.httpStatusCode = 200;
                    }
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to update the provided entry.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<EntryReturnObject<EntryModel>> DeleteEntryItem(int entryId)
        {
            //Init return object
            EntryReturnObject<EntryModel> result = new EntryReturnObject<EntryModel>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    //Delete the phone number entry for the specified entry unique identifier
                    switch (await _entryRepository.Delete(entryId))
                    {
                        //The deletion was successful.
                        case true:
                            //Prep result object accordingly.
                            result.success = true;
                            result.userMessage = "The specified entry was deleted successfully.";
                            result.httpStatusCode = 200;
                            break;
                        //The deletion was unable to take place as the specified unique identier does not exist. 
                        case false:
                            //Prep result object accordingly.
                            result.userMessage = "The entry you are trying to delete does not exist.";
                            result.httpStatusCode = 400;
                            break;
                    }
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error trying to delete the provided entry.";
                result.httpStatusCode = 500;
            }
            return result;
        }

        public async Task<EntryReturnObject<PagedList<Entry>>> SearchEntries(string sortBy, Dictionary<string, string> searchCriteria, int pageNumber, int pageSize, int? phoneBookId)
        {
            EntryReturnObject<PagedList<Entry>> result = new EntryReturnObject<PagedList<Entry>>();
            try
            {
                //The Repository is using this db context. By implementing this using block it will be disposed of accordingly
                using (_context)
                {
                    result.result = (await _entryRepository.SearchEntries(sortBy, searchCriteria, pageNumber, pageSize, phoneBookId));
                    result.success = true;
                    result.userMessage = "Completed search.";
                    result.httpStatusCode = 200;
                }
            }
            catch (Exception error)
            {
                result.error = true;
                result.success = false;
                result.message = "[ERROR] : " + error.Message;
                result.exception = error;
                result.userMessage = "There was an error find the entries you are looking for.";
                result.httpStatusCode = 500;
            }
            return result;
        }
    }
}
