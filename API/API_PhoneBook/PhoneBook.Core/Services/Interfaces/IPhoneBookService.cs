﻿using PhoneBook.Core.Services.ServiceModels;
using PhoneBook.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Core.Services.Interfaces
{
    public interface IPhoneBookService
    {
        /// <summary>
        /// Gets all the Phone books in the database
        /// </summary>
        PhoneBookReturnObject<List<PhoneBookModel>> GetAllPhoneBooks();

        /// <summary>
        /// Adds a phone book to the data base
        /// </summary>
        /// <param name="newPhoneBook">The phone book to be added to the database</param>
        Task<PhoneBookReturnObject<PhoneBookModel>> AddPhoneBook(GeneralPhoneBookModel newPhoneBook);

        /// <summary>
        /// Updates the phone book with the values specified within the GeneralPhoneBookModel object for the specified phone book id.
        /// </summary>
        /// <param name="updatedPhoneBook"></param>
        /// <param name="phoneBookId"></param>
        Task<PhoneBookReturnObject<PhoneBookModel>> UpdatePhoneBook(GeneralPhoneBookModel updatedPhoneBook, int phoneBookId);

        /// <summary>
        /// Deletes the specified phone book and its entries from the database
        /// </summary>
        /// <param name="phoneBookId">The id of the phone book you want to delete and its linked entries</param>
        Task<PhoneBookReturnObject<PhoneBookModel>> DeleteEntryPhoneBook(int phoneBookId);

        /// <summary>
        /// Searchs for Phone books within the database.
        /// </summary>
        /// <param name="sortBy">The column you you want to order by (name)</param>
        /// <param name="searchCriteria">The search values for the columns specified</param>
        /// <param name="pageNumber">The page number of the paginated result</param>
        /// <param name="pageSize">How many returned results per page</param>
        Task<PhoneBookReturnObject<PagedList<Data.Entities.PhoneBook>>> SearchPhoneBooks(string sortBy, Dictionary<string, string> searchCriteria, int pageNumber, int pageSize);
    }
}
