﻿using PhoneBook.Core.Services.ServiceModels;
using PhoneBook.Data.Entities;
using PhoneBook.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Core.Services.Interfaces
{
    public interface IEntryService
    {
        /// <summary>
        /// Adds a new entry to the system
        /// </summary>
        /// <param name="newEntry">The new entry to be added</param>
        Task<EntryReturnObject<EntryModel>> AddEntryItem(GeneralEntryModel newEntry);

        /// <summary>
        /// Updates the Entry for the specified id.
        /// </summary>
        /// <param name="updateEntry">The updated values for the entry</param>
        /// <param name="entryId">The unique identifier for the entry</param>
        Task<EntryReturnObject<EntryModel>> UpdateEntryItem(GeneralEntryModel updateEntry, int entryId);

        /// <summary>
        /// Deletes the entry linked to the unique identifier provided.
        /// </summary>
        /// <param name="entryId">The entry's unique identifier</param>
        Task<EntryReturnObject<EntryModel>> DeleteEntryItem(int entryId);

        /// <summary>
        /// Searchs for Entries within the database
        /// </summary>
        /// <param name="sortBy">The column you you want to order by (name, phone number)</param>
        /// <param name="searchCriteria">The search values for the columns specified</param>
        /// <param name="pageNumber">The page number of the paginated result</param>
        /// <param name="pageSize">How many returned results per page</param>
        /// <param name="phoneBookId">The phone book the entry belongs too</param>
        Task<EntryReturnObject<PagedList<Entry>>> SearchEntries(string sortBy, Dictionary<string, string> searchCriteria, int pageNumber, int pageSize, int? phoneBookId);
    }
}
