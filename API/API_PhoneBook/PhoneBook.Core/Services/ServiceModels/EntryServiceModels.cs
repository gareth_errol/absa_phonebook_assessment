﻿using API_PhoneBook.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Core.Services.ServiceModels
{
    public class EntryModel : GeneralEntryModel
    {
        public Int64 id { get; set; }
    }

    public class GeneralEntryModel
    {

        [Required(ErrorMessage = "This field is required to link your entry to the correct Phone Book")]
        public Int64 entryPhoneBookId { get; set; }

        [Required(ErrorMessage = "This field is required (Name)")]
        [MaxLength(150, ErrorMessage = "This field can not be longer than 150 characters (Name)")]
        public String entryName { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a valid Phone Number")]
        [Required(ErrorMessage = "This field is required (Phone Number)")]
        [MaxLength(20, ErrorMessage = "This field can not be longer than 20 characters (Phone Number)")]
        public String entryPhoneNumber { get; set; }
    }

    //Everything where an entry is related should return this object
    public class EntryReturnObject<T> : BaseReturnObject
    {
        public T result { get; set; }
    }

}
