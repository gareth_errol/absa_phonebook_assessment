﻿using API_PhoneBook.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Core.Services.ServiceModels
{
    public class PhoneBookModel : GeneralPhoneBookModel
    {
        public Int64 id { get; set; }
    }

    public class GeneralPhoneBookModel
    {
        [Required(ErrorMessage = "This field is required (Name)")]
        [MaxLength(150, ErrorMessage = "This field can not be longer than 150 characters (Name)")]
        public String phoneBookName { get; set; }
    }

    public class PhoneBookReturnObject<T> : BaseReturnObject
    {
        public T result { get; set; }
    }
}
