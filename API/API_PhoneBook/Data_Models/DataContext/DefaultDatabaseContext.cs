﻿using Microsoft.EntityFrameworkCore;
using PhoneBook.Data.Entities;

namespace PhoneBook.Data.DataContext
{
    //If you need to migrate this db open the Package Manager Console and select the default project as PhoneBook.Data and the type the following:
    //add-migration first_migration
    //Then type the next command into the Package Manager Console after the build has finished:
    //update-database
    //MAKE SURE THE DB USER IN YOUR APP.SETTINGS FOR THE DB CONNECTION IS CREATED & SET TO SYSTEM ADMIN AS A DB ROLE BEFORE YOU DO THE ABOVE. 
    public class DefaultDatabaseContext : DbContext
    {
        public DefaultDatabaseContext(DbContextOptions<DefaultDatabaseContext> options) : base(options) { }

        public DbSet<Entities.Entry> Entries { get; set; }
        public DbSet<Entities.PhoneBook> PhoneBooks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            #region Entry Table
            modelBuilder.Entity<Entry>().ToTable("entry");
            //Primary Key & Identity Column
            modelBuilder.Entity<Entry>().HasKey(e => e.id);
            modelBuilder.Entity<Entry>().Property(e => e.id).UseIdentityColumn(1, 1).IsRequired();
            //Columns
            modelBuilder.Entity<Entry>().Property(e => e.name).HasMaxLength(150).IsRequired(true);
            //modelBuilder.Entity<Entry>().HasIndex(e => e.name).IsUnique();
            modelBuilder.Entity<Entry>().Property(e => e.phoneNumber).HasMaxLength(20).IsRequired(true);
            //Relationships
            modelBuilder.Entity<Entry>()
                  .HasOne(e => e.phoneBook)
                  .WithMany(p => p.entries)
                  .HasForeignKey(e => e.phoneBookId)
                  .OnDelete(DeleteBehavior.NoAction);
            #endregion

            #region PhoneBook Table
            modelBuilder.Entity<Entities.PhoneBook>().ToTable("phone_book");
            //Primary Key & Identity Column
            modelBuilder.Entity<Entities.PhoneBook>().HasKey(p => p.id);
            modelBuilder.Entity<Entities.PhoneBook>().Property(p => p.id).UseIdentityColumn(1, 1).IsRequired();
            //Columns
            modelBuilder.Entity<Entities.PhoneBook>().Property(p => p.name).HasMaxLength(150).IsRequired(true);
            modelBuilder.Entity<Entities.PhoneBook>().HasIndex(p => p.name).IsUnique();
            //Relationships
            modelBuilder.Entity<Entities.PhoneBook>()
                  .HasMany(p => p.entries)
                  .WithOne(e => e.phoneBook)
                  .HasForeignKey(e => e.phoneBookId)
                  .OnDelete(DeleteBehavior.Cascade);
            #endregion
        }

    }
}
