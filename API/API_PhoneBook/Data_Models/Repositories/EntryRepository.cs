﻿using Microsoft.EntityFrameworkCore;
using PhoneBook.Data.DataContext;
using PhoneBook.Data.Helpers;
using PhoneBook.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Repositories
{
    public class EntryRepository : BaseRepository<Entities.Entry>, IEntryRepository
    {
        public EntryRepository(DefaultDatabaseContext context) : base(context) //Base being the BaseRepository Class 
        {

        }

        public bool NumberExists(Int64 phoneBookId, string phoneNumber)
        {
            return _context.Entries.Any(e => e.phoneBookId.Equals(phoneBookId) && e.phoneNumber.Equals(phoneNumber));
        }

        public async Task<PagedList<Entities.Entry>> SearchEntries(string sortOrder, Dictionary<string, string> searchValues, int? pageNumber, int pageSize, int? phoneBookId)
        {
            string name;
            string phoneNumber;
            bool hasName = searchValues.TryGetValue("name", out name);
            bool hasPhoneNumber = searchValues.TryGetValue("number", out phoneNumber);


            IQueryable<Entities.Entry> searchResults;
            searchResults = from s in _context.Entries
                            select s;
            if (hasName || hasPhoneNumber || phoneBookId > 0)
            {
                if (phoneBookId > 0)
                {
                    searchResults = searchResults.Where(s => s.phoneBookId == (phoneBookId));
                }
                if (hasName)
                {
                    searchResults = from s in searchResults
                                    where s.name.Contains(name)
                                    select s;
                }
                if (hasPhoneNumber)
                {
                    searchResults = from s in searchResults
                                    where s.phoneNumber.Contains(phoneNumber)
                                    select s;
                }
            }
            switch (sortOrder)
            {
                case "name":
                    searchResults = searchResults.OrderByDescending(s => s.name);
                    break;
                case "NAME":
                    searchResults = searchResults.OrderBy(s => s.name);
                    break;
                case "number":
                    searchResults = searchResults.OrderByDescending(s => s.phoneNumber);
                    break;
                case "NUMBER":
                    searchResults = searchResults.OrderBy(s => s.phoneNumber);
                    break;
            }
            return await PagedList<Entities.Entry>.PageResultAsync(searchResults, pageNumber ?? 1, pageSize);


        }
    }
}
