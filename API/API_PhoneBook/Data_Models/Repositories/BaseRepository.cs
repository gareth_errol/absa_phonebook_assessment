﻿using PhoneBook.Data.DataContext;
using PhoneBook.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly DefaultDatabaseContext _context;
        public BaseRepository(DefaultDatabaseContext context)
        {
            _context = context;
        }

        public async Task<T> Create(T objectForDb)
        {
            await _context.AddAsync(objectForDb);
            await _context.SaveChangesAsync();
            return objectForDb;
        }
      
        public async Task<T> Read(Int64 entityId)
        {
            T result = await _context.FindAsync<T>(entityId);
            return result;
        }

        public async Task<List<T>> ReadAll()
        {
            List<T> result = await _context.Set<T>().ToListAsync();
            return result;
        }

        public async Task<T> Update(T objectToUpdate, Int64 entityId)
        {

            T objectFound = await _context.FindAsync<T>(entityId);
            if (objectFound != null)
            {
                _context.Entry(objectFound).CurrentValues.SetValues(objectToUpdate);
                await _context.SaveChangesAsync();
            }
            return objectFound;

        }

        public async Task<bool> Delete(Int64 entityId)
        {
            T recordToDelete = await _context.FindAsync<T>(entityId);
            if (recordToDelete != null)
            {
                _context.Remove(recordToDelete);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

    }
}
