﻿using Microsoft.EntityFrameworkCore;
using PhoneBook.Data.DataContext;
using PhoneBook.Data.Helpers;
using PhoneBook.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Repositories
{
    public class PhoneBookRepository : BaseRepository<Entities.PhoneBook>, IPhoneBookRepository
    {
        public PhoneBookRepository(DefaultDatabaseContext context) : base(context)//Base being the BaseRepository Class 
        {

        }

        public bool PhoneBookNameExists(string phoneBookName)
        {
            return _context.PhoneBooks.Any(p => p.name.Equals(phoneBookName));
        }

        public async Task<PagedList<Entities.PhoneBook>> SearchPhoneBooks(string sortOrder, Dictionary<string, string> searchValues, int? pageNumber, int pageSize)
        {
            string name;
            bool hasName = searchValues.TryGetValue("name", out name);


            IQueryable<Entities.PhoneBook> searchResults;
            searchResults = from s in _context.PhoneBooks
                            select s;
            if (hasName)
            {
                searchResults = from s in searchResults
                                where s.name.Contains(name)
                                select s;
            }
            switch (sortOrder)
            {
                case "name":
                    searchResults = searchResults.OrderByDescending(s => s.name);
                    break;
                case "NAME":
                    searchResults = searchResults.OrderBy(s => s.name);
                    break;
            }
            return await PagedList<Entities.PhoneBook>.PageResultAsync(searchResults, pageNumber ?? 1, pageSize);


        }
    }
}
