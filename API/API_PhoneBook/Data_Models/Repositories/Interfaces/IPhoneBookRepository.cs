﻿using PhoneBook.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Repositories.Interfaces
{
    public interface IPhoneBookRepository : IBaseRepository<Entities.PhoneBook>
    {
        /// <summary>
        /// Checks if the specified phone book for the specified phone book name exists within the database
        /// </summary>
        /// <param name="phoneBookName">The phone book name</param>
        /// <returns></returns>
        bool PhoneBookNameExists(string phoneBookName);
        Task<PagedList<Entities.PhoneBook>> SearchPhoneBooks(string sortOrder, Dictionary<string, string> searchValues, int? pageNumber, int pageSize);
    }
}
