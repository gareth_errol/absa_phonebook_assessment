﻿using PhoneBook.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Repositories.Interfaces
{
    public interface IEntryRepository : IBaseRepository<Entities.Entry>
    {
        /// <summary>
        /// Checks if the specified number for the specified phone book exists within the database
        /// </summary>
        /// <param name="phoneBookId">The phone books id</param>
        /// <param name="phoneNumber">The phone number entry</param>
        /// <returns></returns>
        bool NumberExists(Int64 phoneBookId, string phoneNumber);
        Task<PagedList<Entities.Entry>> SearchEntries(string sortOrder, Dictionary<string, string> searchValues, int? pageNumber, int pageSize, int? phoneBookId);
    }
}
