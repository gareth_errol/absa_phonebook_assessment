﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Entities
{
    public class PhoneBook
    {
        public Int64 id { get; set; }
        public String name { get; set; }

        //Relationships
        //Internal As I dont want to show the entries to my phonebook just the phonebook itself
        internal ICollection<Entry> entries { get; set; }
    }
}
