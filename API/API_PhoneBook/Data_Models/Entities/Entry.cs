﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Data.Entities
{
    public class Entry
    {
        public Int64 id { get; set; }
        public Int64 phoneBookId { get; set; }
        public String name { get; set; }
        public String phoneNumber { get; set; }

        //Relationships
        //Internal As I dont want to show the entry's phonebook itself we already have the phonebooks id.
        internal PhoneBook phoneBook { get; set; }
    }
}
