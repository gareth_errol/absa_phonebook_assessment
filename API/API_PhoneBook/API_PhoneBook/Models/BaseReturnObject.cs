﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_PhoneBook.Models
{
    /// <summary>
    /// This is an abstract class that will provide a standard result set for a return object.
    /// </summary>
    public abstract class BaseReturnObject
    {
        // Constructor
        public BaseReturnObject()
        {
            // Default values
            success = false;
            message = "pending";
            userMessage = string.Empty;
            error = false;
            exception = null;
        }

        public bool success { get; set; }

        internal string message { get; set; }
 
        public string userMessage { get; set; }

        internal bool error { get; set; }

        internal Exception exception { get; set; }

    }
}
