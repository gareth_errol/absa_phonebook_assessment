﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Core.Services.Interfaces;
using PhoneBook.Core.Services.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_PhoneBook.Controllers
{
    [EnableCors("PhoneBookFrontEnd")]
    [Route("api/phonebook")]
    [ApiController]
    public class PhoneBookController : ControllerBase
    {
        private IPhoneBookService _phoneBookService;
        public PhoneBookController(IPhoneBookService phoneBookService)
        {
            _phoneBookService = phoneBookService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetPhoneBooks()
        {
            PhoneBookReturnObject<List<PhoneBookModel>> result = _phoneBookService.GetAllPhoneBooks();
            return StatusCode(result.httpStatusCode, result);
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoneBook([FromBody] GeneralPhoneBookModel phoneBook)
        {
            PhoneBookReturnObject<PhoneBookModel> result = await _phoneBookService.AddPhoneBook(phoneBook);
            return StatusCode(result.httpStatusCode, result);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeletePhoneBook([FromRoute] int id)
        {
            PhoneBookReturnObject<PhoneBookModel> result = await _phoneBookService.DeleteEntryPhoneBook(id);
            return StatusCode(result.httpStatusCode, result);
        }

        [HttpGet]
        public async Task<IActionResult> GetEntries([FromQuery] string sortBy, [FromQuery] int pageSize, [FromQuery] int pageNumber, [FromQuery] string name, [FromQuery] string number)
        {
            Dictionary<string, string> searchCriteria = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "name";
            }
            if (!string.IsNullOrWhiteSpace(name))
            {
                searchCriteria.Add("name", name);
            }
            if (!string.IsNullOrWhiteSpace(number))
            {
                searchCriteria.Add("number", number);
            }
            return Ok(await _phoneBookService.SearchPhoneBooks(sortBy, searchCriteria, pageNumber, pageSize));
        }

        #region EXTRA - NOT IMPLEMENTED
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdatePhoneBook([FromRoute] int id, [FromBody] GeneralPhoneBookModel phoneBook)
        {
            PhoneBookReturnObject<PhoneBookModel> result = await _phoneBookService.UpdatePhoneBook(phoneBook, id);
            return StatusCode(result.httpStatusCode, result);
        }
        #endregion

    }
}
