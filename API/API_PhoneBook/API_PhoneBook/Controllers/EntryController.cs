﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Core.Services.Interfaces;
using PhoneBook.Core.Services.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBook.API.Controllers
{
    [EnableCors("PhoneBookFrontEnd")]
    [Route("api/entry")]
    [ApiController]
    public class EntryController : ControllerBase
    {
        private IEntryService _entryService;
        public EntryController(IEntryService entryService)
        {
            _entryService = entryService;
        }

        [HttpPost]
        public async Task<IActionResult> AddEntryToPhoneBook([FromBody] GeneralEntryModel entry)
        {
            EntryReturnObject<EntryModel> result = await _entryService.AddEntryItem(entry);
            return StatusCode(result.httpStatusCode, result);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteEntryInPhoneBook([FromRoute] int id)
        {
            EntryReturnObject<EntryModel> result = await _entryService.DeleteEntryItem(id);
            return StatusCode(result.httpStatusCode, result);
        }

        [HttpGet]
        public async Task<IActionResult> GetEntries([FromQuery] string sortBy, [FromQuery] int pageSize, [FromQuery] int pageNumber, [FromQuery] string name, [FromQuery] string number, [FromQuery]int? phoneBookId)
        {
            Dictionary<string, string> searchCriteria = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy="name";
            }
            if (!string.IsNullOrWhiteSpace(name))
            {
                searchCriteria.Add("name", name);
            }
            if(!string.IsNullOrWhiteSpace(number))
            {
                searchCriteria.Add("number", number);
            }
            return Ok(await _entryService.SearchEntries(sortBy,searchCriteria,pageNumber,pageSize, phoneBookId));
        }

        #region EXTRA - NOT IMPLEMENTED
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateEntryInPhoneBook([FromRoute] int id, [FromBody] GeneralEntryModel entry)
        {
            EntryReturnObject<EntryModel> result = await _entryService.UpdateEntryItem(entry, id);
            return StatusCode(result.httpStatusCode, result);
        }
        #endregion
    }
}
