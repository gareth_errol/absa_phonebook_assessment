using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PhoneBook.Core.Services.Implementation;
using PhoneBook.Core.Services.Interfaces;
using PhoneBook.Data.DataContext;

namespace API_PhoneBook
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API_PhoneBook", Version = "v1" });
            });

            #region Database
            //For Migrations
            services.AddDbContext<DefaultDatabaseContext>(options =>
                options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection"),
                b => b.MigrationsAssembly(typeof(DefaultDatabaseContext).Assembly.FullName)));
            #endregion

            #region CORS
            services.AddCors();
            string corsUrl = Configuration["Keys:Audience"];
            services.AddCors(options =>
            {
                options.AddPolicy("PhoneBookFrontEnd",
                    builder =>
                    {
                        builder.WithOrigins(corsUrl)
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                    });
            });
            #endregion

            #region My Services
            services.AddScoped<IEntryService, EntryService>();
            services.AddScoped<IPhoneBookService, PhoneBookService>();
            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API_PhoneBook v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
